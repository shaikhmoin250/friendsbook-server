# CLIENT

## Specifications

* Create new posts
* View all posts ordered by upvotes
* Add comments about a given post
* View comments for a given post
* Upvote posts and comments
* We will use mongoose for database connections and to give a strucure to our databse

1. First, we will create a array of post and upvotes then we will display that array by using component.html.

2. Now we will create a function in app.component.ts to push title and upvote when a post button is click, so for that we will create a button called post in our component .html file.

3. We have to enable upvotes so that when a ^ button is clicked it will increment upvotes.
incrementUpvotes = function(post) {
  post.upvotes += 1;
};

4. Now, we will add a link so that a user can give a link along with the post for more deatil about the content of the post.

## Angular Services
1. we will create a service so that the data inside the service can be used by components instead of writing the same code in every component we will create a service and call that that data by using constructor in our component.ts file.

2. We will create a array of posts in service
  posts: []

3. Now we will inject that service in our component.ts file.

## Angular Routing
1. We will create two routes of url /home and /posts/{id} 

2. Now, we will add fake comments data inside addPost funstion of componet.ts

3. Now, Inside our post.html file which has been created for routing which will call the fake comments data.

4. Now we will give the link of route post in component.html file. so that when a user want to comment on a post it routes on another page and then the user can add a comment.

5. Now we will create a function called addComment in our post.component.ts file which will push a comment to a post.

6. Then, we will create a form with one input field to write some comment and one button to add that comment on a specific post.

# SERVER

We will create some route that will help the cleint to interact with 

* view all posts
* Add a new post
* upvote a post
* view comments associated with a post
* add a comment
* upvote a comment

The url of several routes are as follows,

* GET /posts - return a list of posts and associated metadata
* POST /posts - create a new post
* GET /posts/:id - return an individual post with associated comments
* PUT /posts/:id/upvote - upvote a post, notice we use the post ID in the URL
* POST /posts/:id/comments - add a new comment to a post by ID
* PUT /posts/:id/comments/:id/upvote - upvote a comment